"use strict";

const urlModule = require("url"); // The identifier is unconventional in order to avoid clashes with other identifiers.
const workerThreads = require("worker_threads");

const cheerio = require("cheerio");

const utils = require("../src/utils");

class Worker {
    /**
    * Constructor for the Worker class.
    * @param {string} configFilename The filename of the config file. Include the file extension. Do not include the path.
    */
    constructor(configFilename) {
        utils.ensureConfigFileExists(configFilename);
        this.configFilename = configFilename;

        // Get and validate the part of the config for this object.
        this.config = require(`../configs/${configFilename}`).worker;
        this.validateConfig();
        
        // When an HTTP response comes in, it is queued here to be processed later.
        this.responseQueue = [];
        
        // Assign a function to handle messages from the parent thread.
        workerThreads.parentPort.on("message", this.onMessage.bind(this));
    }

    /**
    * The main function of the worker object. This function is recursive. At the end
    * of it's execution it queues itself to be called again in the next cycle of the
    * event loop. Recursing in this way prevents the function from blocking the event
    * loop and allows event driven code to be executed between recursions.
    */
    main() {
        // Process an HTTP response if there is one to be processed from within the
        // response queue. Then send this processed response and some metrics to the parent
        // thread.
        //
        // Only one HTTP response is processed in each recursive call of this function.
        // This ensures that at the start of processing a response, all (event driven)
        // responses have been added to the queue. So the response queue is up-to-date as
        // possible when the number of responses in the queue is send to the parent. The
        // number of responses in the queue is a part of the metrics that are sent to the
        // parent along side the processed response.
        if (this.responseQueue.length > 0) {
            const response = this.responseQueue.shift();

            this.processResponse(response);

            this.sendProcessedResponseAndMetricsToParent(response);
        }

        setImmediate(this.main.bind(this));
    }

    /**
    * A function to deal with incoming messages from the parent thread.
    * @param {Object} message The message object from the parent thread.
    */
    onMessage(message) {
        switch (message.subject) {
            // The parent thread has sent a number of URLs to request. Request them.
            case messageSubjects_in.URLS: {
                for (const url of message.urls) {
                    utils.request(url, this.onResponse.bind(this));
                }

                break;
            }
            
            // An unknown message has been sent from the parent thread. Send back an error.
            default: {
                workerThreads.parentPort.postMessage({
                    subject: messageSubjects_out.ERROR,
                    message: `Unknown message from parent: ${message}.`
                });
            }
        }
    }

    /**
    * A function to be used as the callback function for an HTTP request.
    * @param {Object} response The response from the HTTP request.
    */
    onResponse(response) {
        this.responseQueue.push(response);
    }

    /**
    * A function to process an HTTP response. This function does not create a new
    * response object that is "processed". Instead it processes the response object
    * passed in (by reference) itself.
    * @param {Object} response The response from an HTTP request.
    */
    processResponse(response) {
        // Add a property to the response object to contain links found in the response
        // body.
        response.links = [];

        // If the response status code is 200 (OK) extract the links.
        if (response.statusCode === 200) {
            // Extract all the "a" tags from the response body.
            const $ = cheerio.load(response.body);
            const links = $("a");
            
            // For each "a" tag, extract the HREF link and push it into the links array if
            // suitable.
            links.each((index, link) => {
                let linkHref = $(link).attr("href");

                if (typeof linkHref === "string") {
                    let isPushLink = true;
                    
                    // Do not add link if it is a fragment URL.
                    if (linkHref.charAt(0) === '#') isPushLink = false;
                    
                    // Do not add link if it is a relative URL that starts with a period.
                    if (linkHref.charAt(0) === '.') isPushLink = false;

                    // If the link is a relative URL that starts with a forward slash, prefix it with
                    // the origin.
                    if (linkHref.charAt(0) === '/') {
                        const responseUrlObject = new urlModule.URL(response.url);
                        const origin = responseUrlObject.origin;
                        linkHref = origin + linkHref;
                    }

                    if (isPushLink) response.links.push(linkHref);
                }
            });
        }
        
        // Remove the body from the response object. It is no longer needed and will just
        // add unnecessary overhead when sending the response to the parent.
        delete response.body;
    }
    
    /**
    * A function to send a processed response and some current metrics to the parent.
    * @param {Object} processedResponse A processed response from an HTTP request.
    */
    sendProcessedResponseAndMetricsToParent(processedResponse) {
        workerThreads.parentPort.postMessage({
            subject: messageSubjects_out.RESPONSE_AND_METRICS,
            response: processedResponse,
            metrics: {
                responsesInQueue: this.responseQueue.length
            }
        });
    }

    /**
    * A function to validate the part of the config for this worker object.
    */
    validateConfig() {
        // TODO
    }
}

// A list of valid message subjects for messages coming into the worker thread.
const messageSubjects_in = {
    URLS: 0
}

// A list of valid message subjects for messages leaving the worker thread.
const messageSubjects_out = {
    RESPONSE_AND_METRICS : 0,
    ERROR                : 1
}

module.exports = { Worker, messageSubjects_in, messageSubjects_out };
