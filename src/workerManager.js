"use strict";

const workerThreads = require("worker_threads");

const utils = require("../src/utils");
const worker = require("../src/worker");

class WorkerManager {
    /**
    * Constructor for the WorkerManager class.
    * @param {string} configFilename The filename of the config file. Include the file extension. Do not include the path.
    */
    constructor(configFilename, id) {
        utils.ensureConfigFileExists(configFilename);
        this.configFilename = configFilename;

        // Get and validate the part of the config for this object.
        this.config = require(`../configs/${configFilename}`).workerManager;
        this.validateConfig();

        this.id = id;

        // Create a worker thread and assign a function to handle messages from it.
        this.workerThread = new workerThreads.Worker("./scripts/worker.js", { argv: [this.configFilename] });
        this.workerThread.on("message", this.onMessage.bind(this));

        // The number of URLs the worker thread can be crawling at any given time.
        this.urlCapacity = 1;

        // The number of URLs the worker thread is currently crawling. This number does not
        // include the crawled URLS stored in this object.
        this.urlCount = 0;

        // A flag to indicate whether the URL capacity has settled, i.e. is currently not
        // doubling. Even though upon the initialisation of the object the URL capacity is
        // not doubling, this flag is still set to false. This is because the worker
        // manager has not yet gone through the entire process of determining whether it's
        // URL capacity has settled or not. In addition, an initial value of true will
        // result in a high chance another worker manager will be created in error as the
        // program might mistake this worker as being busy when it is in fact idle.
        this.isUrlCapacitySettled = false;

        // An array to store the crawled URLs before they have been retrieved away from
        // this object.
        this.crawledUrls = [];
    }

    /**
    * A function to deal with incoming messages from the worker thread.
    * @param {Object} message The message object from the worker thread.
    */
    onMessage(message) {
        switch (message.subject) {
            // The worker thread has sent a processed response and some metrics.
            case worker.messageSubjects_out.RESPONSE_AND_METRICS: {
                // Add the processed response, i.e. a crawled URL to the crawled URLs array.
                this.crawledUrls.push(message.response);

                // Update the URL counter to reflect that the worker thread is currently crawling
                // one less URL.
                this.urlCount--;

                // Set the URL capacity settled flag to true. The following code will set it to
                // false if the URL capacity doubles.
                this.isUrlCapacitySettled = true;

                // Check if the response queue in the worker thread is empty. If this is the case,
                // the worker thread is idle.
                if (message.metrics.responsesInQueue === 0) {
                    // Calculate the fraction of the URL capacity, of the worker thread, that is being
                    // used.
                    //
                    // Regarding the "+ 1": When the URL capacity is at it's initial value of 1, the
                    // fraction of the URL capacity being used will always equal 0. This is because the
                    // code only gets here when a URL has been processed and removed from the worker
                    // thread. The fraction of the URL capacity being used being stuck at 0 will prevent
                    // the URL capacity increasing in a later part of the code. To get around this, 1 is
                    // added to the URL count. This does not significantly affect the outcomes after the
                    // URL capacity has increased beyond 1.
                    const fractionOfUrlCapacityBeingUsed = (this.urlCount + 1) / this.urlCapacity;

                    // Use the fraction of the URL capacity being used to determine for what reason the
                    // worker thread is idle. If more than half of the URL capacity is being used and
                    // the worker thread still goes idle, then the URL capacity of the worker thread is
                    // not large enough. In this case, double the URL capacity. If less than half of
                    // the URL capacity is being used when the worker thread goes idle, then the worker
                    // thread simply has not been given enough URLs to crawl. In this case there is
                    // nothing to do.
                    if (fractionOfUrlCapacityBeingUsed >= 0.5) {
                        this.urlCapacity *= 2;

                        console.log(`URL capacity of worker manager doubled. ID: ${this.id}. URL capacity: ${this.urlCapacity}.`);

                        // As the URL capacity has doubled, set the URL capacity settled flag to false.
                        this.isUrlCapacitySettled = false;
                    }
                }

                break;
            }
    
            // The worker thread has messaged an error. Throw the error.
            case worker.messageSubjects_out.ERROR: {
                throw new Error(`Worker thread error: ID: ${this.id}; Error: ${message.message}`);
            }
    
            // An unknown message has been sent from the worker thread. Throw an error.
            default: {
                throw new Error(`Unknown message from worker: ID: ${this.id}; Message: ${JSON.stringify(message.subject)}.`);
            }
        }
    }
    
    /**
    * A getter for the URL capacity.
    * @returns {number} The URL capacity.
    */
    getUrlCapacity() {
        return this.urlCapacity;
    }

    /**
    * A getter for the URL count.
    * @returns {number} The URL count.
    */
    getUrlCount() {
        return this.urlCount;
    }

    /**
    * A function to return the number of how many more URLs the worker able to accept.
    * @returns {number} The URL available capacity.
    */
    getUrlAvailableCapacity() {
        return this.urlCapacity - this.urlCount;
    }

    /**
    * A function to return the flag of whether the URL capacity has settled.
    * @returns {boolean} The flag of whether the URL capacity has settled.
    */
    getIsUrlCapacitySettled() {
        return this.isUrlCapacitySettled;
    }

    /**
    * A function to add URLs that are to be crawled.
    * @param {Array} urls An array of URL strings.
    */
    addUrls(urls) {
        // TODO: Check that too many URLs are not being added.
        
        // Pass the URLs onto the worker thread.
        this.workerThread.postMessage({
            subject: worker.messageSubjects_in.URLS,
            urls: urls
        });

        // Update the URL counter to reflect that the worker thread is currently crawling
        // more URLs.
        this.urlCount += urls.length; 
    }

    /**
    * A function to retrieve URLs that have been crawled. Crawled URLs are objects
    * that contain relevant information on the outcome of the crawl.
    * @returns {Array} An array of crawled URL objects.
    */
    retrieveCrawledUrls() {
        const crawledUrlsToReturn = this.crawledUrls;

        // Empty the crawled URLs array as they will be passed on.
        this.crawledUrls = [];

        return crawledUrlsToReturn;
    }

    /**
    * A function to validate the part of the config for this worker manager object.
    */
    validateConfig() {
        // TODO
    }
}

module.exports = { WorkerManager };
