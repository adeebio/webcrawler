"use strict";

const fs = require("fs");
const urlModule = require("url"); // The identifier is unconventional in order to avoid clashes with other identifiers.

const utils = require("../src/utils");

class Urls {
    /**
    * Constructor for the Urls class.
    * @param {string} configFilename The filename of the config file. Include the file extension. Do not include the path.
    */
    constructor(configFilename) {
        utils.ensureConfigFileExists(configFilename);
        this.configFilename = configFilename;
        
        // Get and validate the part of the config for this object.
        this.config = require(`../configs/${configFilename}`).urls;
        this.validateConfig();

        // Set the hostname from the starting URL.
        const startingUrlObject = new urlModule.URL(this.config.startingUrl);
        this.hostname = startingUrlObject.hostname;

        // Define an object to store the URLs.
        this.urls = {};

        // Define an object to store data about the URLs.
        this.urlsData = {
            counts: {
                total: 0,
                queued: 0,
                requested: 0,
                resolved: 0
            }
        };

        // Add the starting URL to the list of URLs.
        this.addUrls([this.config.startingUrl]);
    }

    /**
    * A function to add URLs to the list of URLs to crawl.
    * @param {Array} urls An array of URL strings.
    */
    addUrls(urls) {
        for (const url of urls) {
            // Only consider adding a URL if the total number of URLs has not exceeded it's
            // limit and the URL and hostname are valid.
            const withinTotalUrlLimit = this.urlsData.counts.total < this.config.totalUrlLimit;
            if (withinTotalUrlLimit && this.isUrlAndHostnameValid(url)) {
                let workingURL = url;
    
                // If the URL has a hash, remove it.
                if (workingURL.includes('#')) {
                    const indexOfHash = workingURL.indexOf('#');
                    workingURL = workingURL.substring(0, indexOfHash);
                }
    
                // If the URL has not already been added, add it.
                if (!this.urls.hasOwnProperty(workingURL)) {
                    this.urls[workingURL] = {
                        url: workingURL,
                        state: urlStates.QUEUED,
                        statusCode: null,
                        headers: null,
                        links: []
                    }

                    this.urlsData.counts.queued++;
                    this.urlsData.counts.total++;
                }
            }
        }
    }

    /**
    * A getter for the total number of URLs.
    * @returns {number} The total number of URLs.
    */
    getUrlCount_total() {
        return this.urlsData.counts.total;
    }

    /**
    * A getter for the number of URLs queued to be requested.
    * @returns {number} The number of URLs queued to be requested.
    */
    getUrlCount_queued() {
        return this.urlsData.counts.queued;
    }
    
    /**
    * A getter for the number of URLs that have been requested.
    * @returns {number} The number of URLs that have been requested.
    */
    getUrlCount_requested() {
        return this.urlsData.counts.requested;
    }

    /**
    * A getter for the number of URLs that have been resolved (/crawled).
    * @returns {number} The number of URLs that have been resolved (/crawled).
    */
    getUrlCount_resolved() {
        return this.urlsData.counts.resolved;
    }

    /**
    * A function for getting a requested number of URLs to be requested / crawled. The
    * number of URLs returned may be less than the requested amount if there aren't
    * that many available.
    * @param {number} numOfUrls The requested number of URLs to get.
    * @returns {Array} An array of URLs to request (/crawl).
    */
    getUrlsForCrawling(numOfUrls) {
        const urlsToReturn = [];

        const listOfUrls = Object.keys(this.urls);
        let listOfUrls_index = 0

        let isUrlsAvailable = listOfUrls.length > listOfUrls_index;
        let isUrlsNeeded = numOfUrls > urlsToReturn.length;

        while (isUrlsAvailable && isUrlsNeeded) {
            const url = this.urls[listOfUrls[listOfUrls_index++]];

            if (url.state === urlStates.QUEUED) {
                url.state = urlStates.REQUESTED;
                this.urlsData.counts.queued--;
                this.urlsData.counts.requested++;
                urlsToReturn.push(url.url);
            }

            isUrlsAvailable = listOfUrls.length > listOfUrls_index;
            isUrlsNeeded = numOfUrls > urlsToReturn.length;
        }

        return urlsToReturn;
    }

    /**
    * A function to add crawled URL objects.
    * @param {Array} crawledUrls An array of crawled URL objects.
    */
    addCrawledUrls(crawledUrls) {
        for (const crawledUrl of crawledUrls) {
            if (this.urls.hasOwnProperty(crawledUrl.url) && this.urls[crawledUrl.url].state === urlStates.REQUESTED) {
                this.urls[crawledUrl.url].statusCode = crawledUrl.statusCode;
                this.urls[crawledUrl.url].headers = crawledUrl.headers;
                this.urls[crawledUrl.url].links = crawledUrl.links;
    
                this.urls[crawledUrl.url].state = urlStates.RESOLVED;
    
                this.urlsData.counts.requested--;
                this.urlsData.counts.resolved++;

                this.addUrls(crawledUrl.links);
            }
        }
    }

    /**
    * A function to check if a URL is a valid URL, with respect to the WHATWG URL
    * Standard, and it's hostname is the same as the starting URL's hostname.
    * @param {string} url The url to check.
    * @returns {boolean} A boolean representing whether the URL is valid or not.
    */
    isUrlAndHostnameValid(url) {
        try {
            const urlObject = new URL(url);
            if (urlObject.hostname === this.hostname) {
                return true;
            } else {
                return false;
            }
        } catch (err) {
            return false;
        }
    }

    /**
    * A function to check if all the URLs have been crawled.
    * @returns {boolean} A boolean representing whether all the URLs have been crawled or not.
    */
    isComplete() {
        return this.urlsData.counts.total === this.urlsData.counts.resolved;
    }

    /**
    * A function to print a sitemap of all the URLs crawled.
    */
    printSitemap() {
        utils.ensurePathExists("./output/");
        fs.writeFileSync(`./output/sitemap.json`, JSON.stringify(this.urls, 0, 4));
    }

    /**
    * A function to validate the part of the config for this urls object.
    */
    validateConfig() {
        // TODO
    }
}

const urlStates = {
    QUEUED    : 0,
    REQUESTED : 1,
    RESOLVED  : 2
}

module.exports = { Urls };
