"use strict";

const urls = require("../src/urls");
const utils = require("../src/utils");
const workerManager = require("../src/workerManager");

class Crawler {
    /**
    * Constructor for the Crawler class.
    * @param {string} configFilename The filename of the config file. Include the file extension. Do not include the path.
    */
    constructor(configFilename) {
        utils.ensureConfigFileExists(configFilename);
        this.configFilename = configFilename;

        // Get and validate the part of the config for this object.
        this.config = require(`../configs/${configFilename}`).crawler;
        this.validateConfig();

        this.urls = new urls.Urls(configFilename);

        this.nextWorkerManagerId = 1;
        this.workerManagers = [];

        // Create one worker manager to start off with.
        this.workerManager_create(1);
    }

    /**
    * The main function of the crawler object. This function is recursive. At the end
    * of it's execution it queues itself to be called again in the next cycle of the
    * event loop. Recursing in this way prevents the function from blocking the event
    * loop and allows event driven code to be executed between recursions.
    */
    main() {
        this.retrieveAndStoreCrawledUrls();
        
        this.createWorkerManagersIfNeeded();

        this.destroyWorkerManagersIfNeeded();

        this.addUrlsToWorkerManagers();

        if (this.urls.isComplete()) {
            this.urls.printSitemap();
            process.exit(0);
        } else {
            setImmediate(this.main.bind(this));
        }
    }

    /**
    * A function to instantiate new Worker Manager objects. This function ensures that
    * no more than the configured max number of worker managers are created.
    * @param {number} numToCreate The number of Worker Manager objects to instantiate.
    */
    workerManager_create(numToCreate) {
        for (let i = 0; i < numToCreate; i++) {
            if (this.workerManagers.length < this.config.maxNumOfWorkers) {
                const id = this.nextWorkerManagerId++;
                const workerManagerObject = new workerManager.WorkerManager(this.configFilename, id);
                this.workerManagers.push(workerManagerObject);
                console.log(`New worker manager created. ID: ${id}.`);
            }
        }
    }

    /**
    * A function to destroy a list of Worker Manager objects.
    * @param {Array} ids An array of IDs of Worker Manager objects to destroy.
    */
    workerManager_destroy(ids) {
        // TODO
    }

    /**
    * A function to collect crawled URLs and pass them onto the Urls object.
    */
    retrieveAndStoreCrawledUrls() {
        for (const workerManagerObject of this.workerManagers) {
            const crawledUrls = workerManagerObject.retrieveCrawledUrls();
            this.urls.addCrawledUrls(crawledUrls);
        }
    }

    /**
    * A function to create worker managers (hence worker threads) if it would speed
    * up crawling and not result in existing worker managers idling.
    */
    createWorkerManagersIfNeeded() {
        let isAllUrlCapacitiesSettled = true;

        for (const workerManagerObject of this.workerManagers) {
            if (!workerManagerObject.getIsUrlCapacitySettled()) {
                isAllUrlCapacitiesSettled = false;
            }
        }

        // Only consider creating a new worker manager if none of the existing worker
        // managers are increasing in URL capacity.
        if (isAllUrlCapacitiesSettled) {
            let totalUrlCapacity = 0;

            for (const workerManagerObject of this.workerManagers) {
                totalUrlCapacity += workerManagerObject.getUrlCapacity();
            }

            const averageUrlCapacity = totalUrlCapacity / this.workerManagers.length;

            // Create a new worker manager only if there are enough URLs queued so that a new
            // worker manager would not be idle. It is assumed that the new worker manager
            // would have a URL capacity equal to the average of the other worker managers.
            if (this.urls.getUrlCount_queued() > averageUrlCapacity) {
                this.workerManager_create(1);
            }
        }
    }

    /**
    * A function to destroy worker managers if needed.
    */
    destroyWorkerManagersIfNeeded() {
        // TODO
    }

    /**
    * A function to add URLs to crawl to worker managers.
    */
    addUrlsToWorkerManagers() {
        for (const workerManagerObject of this.workerManagers) {
            const numOfUrlsToGet = workerManagerObject.getUrlAvailableCapacity();
            const urlsForCrawling = this.urls.getUrlsForCrawling(numOfUrlsToGet);
            workerManagerObject.addUrls(urlsForCrawling);
        }
    }

    /**
    * A function to validate the part of the config for this crawler object.
    */
    validateConfig() {
        // TODO
    }
}

module.exports = { Crawler };
