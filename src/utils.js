"use strict";

const fs = require("fs");
const http = require("http");
const https = require("https");
const urlModule = require("url"); // The identifier is unconventional in order to avoid clashes with other identifiers.

module.exports = {
    /**
    * A function to ensure that the given config file exits.
    * @param {string} configFilename The filename of the config file. Include the file extension. Do not include the path.
    */
    ensureConfigFileExists: function (configFilename) {
        if (configFilename === undefined) {
            throw new Error(`The given config filename is undefined. Check that a config filename was given when the script was executed.`);
        }

        if (!fs.existsSync(`./configs/${configFilename}`)) {
            throw new Error(`The given config file does not exist: ${configFilename}.`);
        }
    },

    /**
    * A function to check if a URL is valid with respect to the WHATWG URL Standard.
    * @param {string} url The URL to check.
    */
    isUrlValid: function (url) {
        try {
            new URL(url);
            return true;
        } catch (err) {
            return false;
        }
    },

    /**
     * A simple HTTP(S) GET request function.
     * @param {string} url The URL to request. It must include the origin (as per the WHATWG URL Standard).
     * @param {callback} [callback] An optional callback that takes in a response object (see func. definition).
     */
    request: function (url, callback = (response) => {}) {
        // TODO: Validate params.

        // An object to contain the parts response that will be passed on to the callback
        // function.
        const response = {
            url        : url,
            statusCode : null,
            headers    : null,
            body       : ""
        };

        // Determine what request module to use according to the URL's protocol.
        const urlObject = new urlModule.URL(url);
        let requestModule = null;
        switch (urlObject.protocol) {
            case "http:": {
                requestModule = http;
                break;
            }

            case "https:": {
                requestModule = https;
                break;
            }

            default: {
                throw new Error(`Invalid URL protocol: ${urlObject.protocol}; URL: ${url}.`);
            }
        }
    
        const req = requestModule.request(url, {}, (res) => {
            res.on("data", (chunk) => {
                response.body += chunk;
            });
    
            res.on("end", () => {
                response.statusCode = res.statusCode;
                response.headers    = res.headers;
    
                callback(response);
            });
        });
        
        req.on("error", (err) => {
            throw new Error(`Request error: Name: ${err.name}; Message: ${err.message}.`);
        });
        
        req.end();
    },

    /**
     * A function to ensure a path exists.
     * @param {string} path The path to ensure exists.
     */
    ensurePathExists: function (path) {
        for (let i = 0; i < path.length; i++) {
            if (path[i] === "/" || i === path.length - 1) {
                const pathPart = path.substr(0, i + 1);
    
                if (!fs.existsSync(pathPart)) fs.mkdirSync(pathPart);
            }
        }
    }
};
