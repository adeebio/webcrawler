"use strict";

const workerThreads = require("worker_threads");

const worker = require("../../src/worker");

if (workerThreads.isMainThread) {
    const worker = new workerThreads.Worker(__filename);
    worker.on("message", (message) => { if (message === "exit") process.exit(0); });
} else {
    const response = {
        url: "https://example.com/",
        statusCode: 200,
        body:
`
<!DOCTYPE html>
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <a href="https://example.com/page1.html">link text</a>
        <a href="https://example.com/path/page2.html">link text</a>
        <a href="http://example.com/page3.html">link text</a>
        <a href="/page4.html">link text</a>
        <a href="./page5.html">link text</a>
        <a href="../page6.html">link text</a>
    </body>
</html>
`
    };

    const workerObject = new worker.Worker("test.js");

    workerObject.processResponse(response);

    console.log(response.links);
    
    workerThreads.parentPort.postMessage("exit");
}
