"use strict";

const fs = require("fs");

function deleteFolderRecursive(path) {
    if(fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function(file, index) {
            const curPath = path + "/" + file;
            
            if(fs.lstatSync(curPath).isDirectory()) {
                deleteFolderRecursive(curPath);
            } else {
                fs.unlinkSync(curPath);
            }
        });

        fs.rmdirSync(path);
    }
};

deleteFolderRecursive("./node_modules");
deleteFolderRecursive("./output");
