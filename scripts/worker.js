"use strict";

const process = require("process");

const workerModule = require("../src/worker"); // The identifier is unconventional in order to avoid clashes with other identifiers.

const worker = new workerModule.Worker(process.argv[2]);

worker.main();
