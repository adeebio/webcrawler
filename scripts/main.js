"use strict";

const process = require("process");

const crawlerModule = require("../src/crawler"); // The identifier is unconventional in order to avoid clashes with other identifiers.

const crawler = new crawlerModule.Crawler(process.argv[2]);

crawler.main();
