# Web crawler - Adeeb Ahmed Hossain

## Prerequisites

- Node.js version 14 (and the included NPM) or higher.

## Use

- Install dependencies:
    - `$ npm install`
- Edit the config file as necessary:
    - _./configs/main.js_
- Run the web crawler:
    - `$ node ./scripts/main.js main.js`
        - where `./scripts/main.js` is the path to the web crawler script.
        - where `main.js` is the name of config file to load.
- If successful, the sitemap output can be found:
    - _./output/sitemap.json_

## Key design features

- The web crawler uses worker threads to make the HTTP requests and parse the responses.
- When initialised, worker threads have a capacity to request one HTTP request at a time. However, this capacity is automatically increased until each worker thread is requesting enough HTTP requests such that it is kept busy all the time processing the HTTP responses.
- New worker threads are started when all existing worker threads are busy and a new worker thread will increase the speed of the web crawler.
    - Note that the number of effective threads is limited to the number of cores on a machine.
    - As a result it is recommended to limit the maximum number of worker threads, in the config file, to the number of cores on the machine.
- URL fragments are ignored as they do not lead to different pages.

## Interesting points

- When crawling a regular website, only a few worker threads get started as most of the links are repeated across pages.
- However, on a site with many unique links, like a Wikipedia page, many threads are started up to deal with the huge surge in unique links that need to be crawled.

## Bugs and limitations

- Only absolute or relative paths that start with a forward slash are followed up upon.
- All HTTP responses are treated as HTML responses (even if they are images, PDFs, etc).
- Only HTTP responses with a status code of 200 are processed.
- Redirects, etc are not processed.

## Tests

- The web crawler has been tested manually on a number of websites.
- As far as units tests, only a proof of concept has been written in:
    - _./scripts/tests/worker\_processResponse.js_