"use strict";

module.exports = {
    crawler: {
        //
        // The maximum number of workers (worker threads) the crawler can use.
        //
        // maxNumOfWorkers: 1
        maxNumOfWorkers: 100
        // maxNumOfWorkers: require("os").cpus().length
    },

    urls: {
        //
        // The starting URL from which to crawl the website. The URL must be valid with
        // respect to the WHATWG URL Standard. See the Node.js documentation for
        // information:
        //
        //     https://nodejs.org/api/url.html#url_url_strings_and_url_objects
        //
        startingUrl: "https://www.javascript.com/",
        // startingUrl: "https://en.wikipedia.org/wiki/JavaScript",

        //
        // The total number of URLs to crawl.
        //
        totalUrlLimit: 500
    },

    workerManager: {
        // No configurables.
    },

    worker: {
        // No configurables.
    }
};
